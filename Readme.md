# Personalización i3wm

![I3Wm](./img/Screenshot-01.png)

## Instalación

```bash
# Instalamos dependencias.
sudo apt-get install i3 dunst compton tilix feh rofi system-config-printer

# Bajamos configuración.
git clone git@gitlab.com:edumag/i3-configuraci-n.git
mv i3-configuraci-n.git $HOME/.config/i3

# Instalamos font awesome.
wget https://github.com/creationix/boxcode/raw/master/font/fontawesome-webfont.ttf
[[ -d "$HOME/.fonts" ]] || mkdir $HOME/.fonts
mv fontawesome-webfont.ttf $HOME/.fonts/

# Fuente System San Francisco
wget https://github.com/supermarin/YosemiteSanFranciscoFont/blob/master/System%20San%20Francisco%20Display%20Regular.ttf?raw=true -o "$HOME/.fonts/System San Francisco Display Regular.ttf"

# Añadimos configuración propia a dunst
ln -s "$HOME/.config/i3/dunst" "$HOME/.config"

# Control de volumen
sudo apt-get install pasystray lxappareance pavucontrol
```



## Combinación de teclas básica

| Combinación      | Acción                                      |
| ---------------- | ------------------------------------------- |
| Mod+Enter        | Terminal                                    |
| Mod+Shift+Q      | Cerrar centana                              |
| Mod+d            | Lanzador aplicaciones                       |
| Mod+Space        | Cambiar ventana a modo flotante             |
| Mod+r            | Redimensionar ventana                       |
| Mod+Alt_L        | Seleccionar ventana con Rofi                |
| Mod+Shift+Flecha | Mover ventana de posición                   |
| Mod+w            | Cambio de disposición de ventanas (Layouts) |
| Mod+Flecha       | Cambiamos foco de ventana                   |
| Mod+f            | FullScreen                                  |
| Mod+p            | Gestión de impresoras                       |

## font-awesome

[Font Awesome Free's Cheatsheet](https://fontawesome.com/cheatsheet?from=io)

Desde la web de fontawesome podemos copiar y pegar los iconos en nuestros
ficheros de configuración

## Rofi

Instalamos rofi como lanzador de aplicación
y de paso tenemos calculadora y selector de
ventanas.

### Menú de aplicaciones (Mod+d)

![Rofi1](./img/i3wm-02.png)

### Calculadora (Mod+c)

![Rofi2](./img/i3wm-03.png)

### Seleccionar ventana. (Mod+Tab_L)

![Rofi3](./img/i3wm-04.png)

## dunst (Notificaciones)

![Rofi3](./img/i3wm-05.png)

## pasystray (Volumen)

Al lanzar pasystray el icono oscuro no se ve bien.
Lanzamos lxappareance para cambiar el tema gtk y los iconos.

![Rofi3](./img/i3wm-06.png)

## Configuración de touchpad.

```
sudo mkdir -p /etc/X11/xorg.conf.d && sudo tee <<'EOF' /etc/X11/xorg.conf.d/90-touchpad.conf 1> /dev/null
Section "InputClass"
        Identifier "touchpad"
        MatchIsTouchpad "on"
        Driver "libinput"
        Option "Tapping" "on"
EndSection

EOF
```
### Referencias

- https://cravencode.com/post/essentials/enable-tap-to-click-in-i3wm/

## Control de brillo

No he podido encontrar ninguna aplicación que me funcione, al final he realizado un simple script que hace la función.

> $ ./brightness.sh -h
>
> uso: brightness.sh [+|-]

En mi caso:

Añadir "acpi_backlight=vendor" a la linea 

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
```

en /etc/default/grub y ejecutamos 

`sudo update-grub2.`


## Referencias

- [Una buena explicación de como funciona](https://github.com/bookercodes/dotfiles.git)
- https://sergioquijanorey.github.io/i3/linux/programacion/administracion/2018/09/09/configuraci%C3%B3n-i3wm.html



